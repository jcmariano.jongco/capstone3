<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product Page</title>
    <style>
        
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        .product-list {
            display: flex;
            justify-content: space-between;
            margin-bottom: 20px;
        }

        .product-card {
            border: 1px solid #ccc;
            padding: 10px;
            cursor: pointer;
        }

        .product-card:hover {
            background-color: #f0f0f0;
        }

        .product-details {
            display: none;
        }

        img {
            max-width: 100%;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Product Page</h1>
        
        <div class="product-list">
            <div class="product-card" onclick="showProduct(1)">Product 1</div>
            <div class="product-card" onclick="showProduct(2)">Product 2</div>
            <div class="product-card" onclick="showProduct(3)">Product 3</div>
        </div>
        
        <div class="GIANT TCR ADVANCED DISC 1 KOM 2023" id="product-1">
            <h2>Product 1</h2>
            <img src="product1.jpg" alt="MY22DefyAdvanced0_ColorACarbon_Terracotta.jpg">
            <p>Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <p>Price: $99</p>
        </div>

        <div class="GIANT DEFY ADVANCED 2 2022" id="product-2">
            <h2>Product 2</h2>
            <img src="product2.jpg" alt="MY22TCRAdvanced2Disc-PC_ColorACarbon.jpg">
            <p>Description: Vivamus at varius magna.</p>
            <p>Price: $79</p>
        </div>

        <div class="GIANT TCR ADVANCED DISC 3 2022" id="product-3">
            <h2>Product 3</h2>
            <img src="product3.jpg" alt="MY23TCRAdvanced1Disc-PCXColorA_ColdNight_Front.jpg">
            <p>Description: Phasellus id lorem ut libero euismod feugiat sit amet eget ligula.</p>
            <p>Price: $149</p>
        </div>
    </div>

    <script>
        function showProduct(productId) {
            // Hide all product details
            const productDetails = document.querySelectorAll('.product-details');
            productDetails.forEach((product) => {
                product.style.display = 'none';
            });

            const selectedProduct = document.getElementById(`product-${productId}`);
            selectedProduct.style.display = 'block';
        
    </script>
</body>
</html>
