import React, { useLayoutEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { setUser } from '../store/reducers/authSlice';
import { useCheckOutMutation } from '../services/orderApi';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import './modal.css'

// import ProductImage1 from '../images/product1.jpg'; // Import your product images
import Login from './Login';
import { useDeleteProductsMutation, useGetProductQuery, useUpdateProductsMutation, useGetActiveProductQuery } from '../services/productApi';
import AddProductComponent from './AddProducts';
import EditProductComponent from './EditProducts';
import { useGetOrderQuery } from '../services/orderApi';

const HomeClient = () =>  {
    const myElement = {
        // Modal container
        modal: {
        display: 'flex',
          position: 'fixed',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0, 0, 0, 0.5)', /* Semi-transparent background */
          zIndex: 1000, /* Ensure it appears on top of other content */
          justifyContent: 'center',
          alignItems: 'center',
        },
      
        // Modal content
        modalContent: {
          backgroundColor: '#fff',
          padding: '20px',
          borderRadius: '5px',
          boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.2)',
          maxWidth: '400px',
          width: '100%',
          textAlign: 'center',
        },
      
        // Close button (optional)
        modalClose: {
          position: 'absolute',
          top: '10px',
          right: '10px',
          cursor: 'pointer',
        },
      
        // Style the header, content, and buttons inside the modal as needed
      
        // Add button style
        modalAddButton: {
          backgroundColor: '#007bff',
          color: '#fff',
          border: 'none',
          padding: '10px 20px',
          marginRight: '10px',
          borderRadius: '5px',
          cursor: 'pointer',
        },
      
        // Close button style (optional)
        modalCloseButton: {
          backgroundColor: '#ccc',
          color: '#333',
          border: 'none',
          padding: '10px 20px',
          borderRadius: '5px',
          cursor: 'pointer',
        },
      
        // Additional styling can be added for different parts of the modal as needed
      };
  const userData = localStorage.getItem('userData');
  const [totalPrice, setTotalPrice] = React.useState(0);
  const [cart, setCart] = React.useState([]);
  const [productQuantities, setProductQuantities] = React.useState({}); 
  const [checkOut] =useCheckOutMutation();
  const [editForm, setEditForm] = React.useState(false);
  const [editParams, setEditParams] = React.useState([]);
  const auth = useSelector((state) => state.auth)
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { data: getActiveProducts } = useGetActiveProductQuery();
  const users = JSON.parse(localStorage.getItem('users')) || [];
  const activeUser = users.find((user) => user.active === true) || [];

    const [showModal, setShowModal] = React.useState(false); // State to control the modal visibility
    const [selectedProduct, setSelectedProduct] = React.useState(null); // State to store the selected product
  
    const handleQuantityInputChange = (productId, event) => {
      const newQuantity = parseInt(event.target.value);
      setProductQuantities({
        ...productQuantities,
        [productId]: newQuantity,
      });
    };
  
    const addToCart = (product) => {
        if (!activeUser) {
          navigate('/register');
        } else {
          const quantity = productQuantities[product._id] || 1;
          const existingCartItemIndex = cart.findIndex((item) => item.id === product._id);
    
          if (existingCartItemIndex !== -1) {
            // If the product is already in the cart, update its quantity
            const updatedCart = [...cart];
            updatedCart[existingCartItemIndex].quantity += quantity;
            setCart(updatedCart);
          } else {
            // If the product is not in the cart, add it with the specified quantity
            const newItem = { ...product, quantity };
            setCart([...cart, newItem]);
          }
    
          // Close the modal after adding to cart
          setShowModal(false);
          setSelectedProduct(null);
        }
      };
  
    const openModal = (product) => {
      setSelectedProduct(product);
      setShowModal(true);
    };
  
    const closeModal = () => {
      setShowModal(false);
      setSelectedProduct(null);
    };

  

  const productItemStyle = {
    border: '1px solid #ccc',
    borderRadius: '5px',
    padding: '10px',
    margin: '10px',
    width: '300px',
    textAlign: 'center',
    display: 'inline-block',
  };

  const handleCheckout = async () => {
    const productIDS = cart.map((item) => ({ productId: item.id, quantity: item.quantity, productName: item.productName }))
    const dataForm = {
        totalAmount: totalPrice,
        userId: auth.id,
        products: productIDS
    }
    await checkOut(dataForm);
    setCart([]);
  }

  const handleLogout = () => {
    dispatch(setUser({access: '', email: ''}))
    localStorage.removeItem('access');
    localStorage.removeItem('email');
    localStorage.removeItem('admin');
    navigate('/');
  };
  const calculateTotalAmount = () => {
    const price = cart.reduce((total, item) => total + item.price, 0).toFixed(2)
    return price;
  };
  React.useEffect(() => {
    const price = cart.reduce((total, item) => total + item.price, 0).toFixed(2);
    setTotalPrice(price);
  }, [cart]);
  
  return (
    <div>
      <div style={{display: 'flex', justifyContent: 'space-evenly'}}>
      {!auth.isAdmin ? (<h2>Admin Dashboard</h2>) : (<h2>Client</h2>)}
      <div>
      {auth && auth?.email ? (
        <>  <p>Welcome, {auth?.email}</p>
        <Button variant="secondary" onClick={handleLogout}>Log out!</Button>
        </>

) : (
  <Login />
)}
      </div>
      {cart ? (<div>
        <div>
      <h1>Shopping Cart</h1>
      {/* <button onClick={addToCart}>Add Sample Product to Cart</button> */}

      {/* Cart Items */}
      <div>
        <h2>Cart Items</h2>
        <ul>
          {cart.map((item) => (
            <li key={item._id}>
              {item.productName} - ${item.price.toFixed(2)}
            </li>
          ))}
        </ul>
        <Button variant='success' onClick={handleCheckout}>Checkout</Button>
      </div>

      {/* Total Amount */}
      <div>
        <h2>Total Amount</h2>
        <p>${calculateTotalAmount()}</p>
      </div>
    </div>
      </div>) : (<div>No Item</div>)}
      </div>
        <div>
        <div className="product-list">
          <Container>
                  <Row>
                 {getActiveProducts && getActiveProducts?.map((product) => (
                  
<Col key={product._id} xs={12} sm={6} md={4} lg={3} style={{marginBottom: 20}}>
                  <Card  style={{ width: '18rem' }}>
                    <Card.Body>
                      <Card.Title>{product.productName}</Card.Title>
                      <Card.Text>{product.description}</Card.Text>
                      <Card.Text>{product.price}</Card.Text>
                      <div style={{ display: 'flex', justifyContent: 'center' }}>
                     <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Form.Label fhtmlFor="quantity" style={{paddingRight: '4rem'}}>Quantity</Form.Label>
              <Form.Control
                type="number"
                name="quantity"
                value={productQuantities[product._id] || 1} // Use the quantity from state or default to 1
                onChange={(event) => handleQuantityInputChange(product._id, event)}
              />
            </div>
</div>
                  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '10px'}}>
                  <Button variant='danger'
                    onClick={() => openModal(product)}
                  >
                    Add to Cart
                  </Button>
                  </div>
                    </Card.Body>
                  </Card></Col>

                 ))}</Row></Container>
               </div>
    </div>
    {showModal && (
        <div style={myElement.modal} className="modal">
          <div style={myElement.modalContent} className="modal-content">
            <h2>Add to Cart</h2>
            <p>Product: {selectedProduct.productName}</p>
            <p>Price: ${selectedProduct.price}</p>
            <p>Quantity: {productQuantities[selectedProduct._id] || 1}</p>
            <button onClick={() => addToCart(selectedProduct)}>Add</button>
            <button onClick={closeModal}>Close</button>
          </div>
        </div>
      )}
    </div>

    
  );
  
  
}



export default HomeClient;
