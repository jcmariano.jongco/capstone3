<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Retrieve Products</title>
</head>
<body>
    <ul id="product-list"></ul>

    <script>
        // Function to fetch and display products
        function fetchProducts() {
            
            fetch('products.json')
                .then(response => response.json())
                .then(products => {
                    const productList = document.getElementById('product-list');
                    products.forEach(product => {
                        const listItem = document.createElement('li');
                        listItem.textContent = `Product Name: ${product.name}, Price: $${product.price}`;
                        productList.appendChild(listItem);
                    });
                })
                .catch(error => {
                    console.error('Error fetching products:', error);
                });
        }

        // Call the fetchProducts function when the page loads
        window.addEventListener('load', fetchProducts);
    </script>
</body>
</html>
