import React, { useLayoutEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { setUser } from '../store/reducers/authSlice';
import Login from './Login';
import { useDeleteProductsMutation, useGetProductQuery, useUpdateProductsMutation, useGetActiveProductQuery } from '../services/productApi';
import AddProductComponent from './AddProducts';
import EditProductComponent from './EditProducts';
import { useGetOrderQuery } from '../services/orderApi';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Table from 'react-bootstrap/Table';

const Home = () =>  {
  // const userData = localStorage.getItem('userData');
  // const [cart, setCart] = React.useState([]);
  const [editForm, setEditForm] = React.useState(false);
  const [editParams, setEditParams] = React.useState([]);
  const [view, setView] = React.useState(false);
  const auth = useSelector((state) => state.auth)
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { data: getProducts } = useGetProductQuery();
  const { data: getOrders } = useGetOrderQuery();
  const { data: getActiveProducts } = useGetActiveProductQuery();
  const [updateProducts] = useUpdateProductsMutation();
  const [deleteProducts] = useDeleteProductsMutation();
  const users = JSON.parse(localStorage.getItem('users')) || [];
  const activeUser = users.find((user) => user.active === true) || [];
  console.log('REDCCC', auth.isAdmin)
  if (activeUser) {
      // There is an active user, you can access their information
      console.log('Active user:', activeUser.username);
    } else {
      // There is no active user
      console.log('No active user found');
    }

  // const addToCart = (id) => {
  //   console.log(userData)
  //   if (!activeUser) {
  //       navigate('/register')
  //   } else {
  //       if(cart.includes(id)) {
  //           alert('Item is already in the cart');
  //       } else {
  //           setCart([...cart, id])
  //       }
  //   }
  // }

  

  const productItemStyle = {
    border: '1px solid #ccc',
    borderRadius: '5px',
    padding: '10px',
    margin: '10px',
    width: '300px',
    textAlign: 'center',
    display: 'inline-block',
  };

  const handleLogout = () => {
    dispatch(setUser({access: '', email: ''}))
    localStorage.removeItem('access');
    localStorage.removeItem('email');
    localStorage.removeItem('admin');
    navigate('/');
  };

  const handleActive = async (prams) => {
    if(prams?.isActive) {
      const { data } = await updateProducts({productName: prams.productName, description: prams.description, price: prams.price, id: prams._id, isActive: false})
      console.log(data);
    } else {
      const { data } = await updateProducts({productName: prams.productName, description: prams.description, price: prams.price, id: prams._id, isActive: true})
    }
    window.location.reload();
  }

  const deleteProduct = async (prams) => {
    const results = window.confirm("Do you want to continue?");

    if (results) {
      const { data } = await deleteProducts({id: prams});
      alert("Product Succefully Delete!");
      console.log(data)
      window.location.reload();
    } else {
      alert("You Clicked No");
    }
  }
  const editUpdate = (prams) => {
    if (prams) {
      setEditParams(prams)
      setEditForm(true)
    } else {
      setEditForm(false)
    }
  }
  
  useLayoutEffect(() => {
    console.log(getProducts, auth, getOrders)
  })

  return (
    <div style={{ margin: '2rem'}}>

      <div style={{display: 'flex', justifyContent: 'space-evenly', alignItems:"center"}}>
      {auth.isAdmin ? (<h2>Admin Dashboard</h2>) : (<h2>Client</h2>)}
      <div>
      {auth && auth?.email ? (
        <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'baseline', gap: '3rem'}}>  <p>Welcome, {auth?.email}</p>
        <Button variant='secondary' onClick={handleLogout}>Log out!</Button>
      
        </div>
        

) : (
  <Login />
)}
      </div>

      </div>
      <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand>Admin</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link onClick={() => {setView(true)}}>Orders</Nav.Link>
            <NavDropdown title="Dropdown" id="basic-nav-dropdown">
              <NavDropdown.Item onClick={() => {setView(false)}}>Products</NavDropdown.Item>
              {/* <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item> */}
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    {auth.isAdmin ? (<>
          {view && auth.isAdmin ? <>
            <div>
              {getOrders.map((order) => (
                <div>
                <Table style={{borderCollapse: 'collapse', width: '100%'}}>
                  <thead>
                    <tr>
                      <th style={{border: '1px solid #dddddd', textAlign: 'left', padding: 8 }}>User Id</th>
                      <th style={{border: '1px solid #dddddd', textAlign: 'left', padding: 8 }}>Date Purchased</th>
                      <th style={{border: '1px solid #dddddd', textAlign: 'left', padding: 8 }}>Product</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style={{border: '1px solid #dddddd', textAlign: 'left', padding: 8 }}>{order.userId}</td>
                      <td style={{border: '1px solid #dddddd', textAlign: 'left', padding: 8 }}>{order.purchasedOn}</td>
                      {order.products.map((i) => {
                        return (
                          <tr>
                            <td  style={{border: '1px solid #dddddd', textAlign: 'left', padding: 8 }}>{i._id}</td>
                          </tr>
                        )
                      })}
                    </tr>
                  </tbody>
                </Table>
                {/* <div>{order.userId}</div>
                <div>{order.purchasedOn}</div> */}
                </div>
              ))}
            </div>
          </> : (
        <>
                  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                {editForm ?  <h1>Edit Product</h1>:  <h1>Add Product</h1>}
               {editForm ? <EditProductComponent params={editParams} /> : <AddProductComponent />}
               </div>
               <div className="product-list" style={{display: 'flex'}}>
                <Container>
                <Row>
                 {getProducts && getProducts?.map((product) => (

                    <Col key={product._id} xs={12} sm={6} md={4} lg={3} style={{marginBottom: 20}}>
                  <Card  style={{ width: '18rem' }}>
                    <Card.Body>
                    <div style={{display: 'flex', justifyContent: 'end', gap: 20}}>
                      <Button onClick={() => editUpdate(product)} variant="primary">Edit</Button>
                      <Button onClick={() => deleteProduct(product._id)} variant="danger">X</Button>
                    </div>
                      <Card.Title>{product.productName}</Card.Title>
                      <Card.Text>{product.description}</Card.Text>
                      <Card.Text>{product.price}</Card.Text>
                      <Form.Check
                          type="checkbox"
                          label="Enable"
                          checked={product.isActive}
                          onChange={() => handleActive(product)}
                        />
                    </Card.Body>
                  </Card></Col>
                 ))}</Row>
                 </Container>
               </div>
        </>
    )}
    </>): (<div>
      {console.log('sss')}
    </div>)}
    </div>
  );
}

export default Home;
