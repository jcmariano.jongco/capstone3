import { useState, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import 'bootstrap/dist/css/bootstrap.min.css';


import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {


	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
			<Container fluid>
			    <Navbar.Brand as={Link} to="/">Padyak Manila</Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
				    <Nav className="ms-auto">
				        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
				        <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
				        {(user.id !== null) ? 

								user.isAdmin 
								?
								<>
									<Nav.Link as={Link} to="/addProduct">Add Course</Nav.Link>
									<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
								</>
								:
								<>
									<Nav.Link as={Link} to="/profile">Profile</Nav.Link>
									<Nav.Link as={Link} to="/searchByPrice">Search by Price</Nav.Link>
									<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
								</>
							: 
								<>
									<Nav.Link as={Link} to="/login">Login</Nav.Link>
									<Nav.Link as={Link} to="/register">Register</Nav.Link>
								</>
						}
				    </Nav>
			    </Navbar.Collapse>
			</Container>
		</Navbar>
		)
}