import React from 'react';
import { useParams } from 'react-router-dom';

const Product = () => {
  const { id } = useParams(); // Get the product ID from the URL

  // In a real application, you would fetch product details by ID

  return (
    <div>
      <h2>Product Details</h2>
      <p>Product ID: {id}</p>
      {/* Display product details here */}
    </div>
  );
}

export default Product;
