import {useState, useEffect} from 'react';
import { useAddProductsMutation, useUpdateProductsMutation } from '../services/productApi';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

const EditProductComponent = ({params}) => {

    const [itemData, setItemData] = useState({ id: params?._id, name: params?.productName, description: params?.description, price: params?.price});
    const handleInputChange = (e) => {
        const { name, value } = e.target;
        console.log(itemData)
        setItemData({
          ...itemData,
          [name]: value,
        });
      };
    const [updateProducts] = useUpdateProductsMutation();
    const addProduct = async () => {
        try {
            const { code, status, data } = await updateProducts({...itemData})
            alert('Successfully Added!', data, status, code);
            console.log(updateProducts)
            window.location.reload();
        } catch (error){
            console.log(error);
        }
    }

    useEffect(() => {
        console.log(itemData)
    }, [itemData])

    return (
        <div style={{ height: 100, padding: 25}}>
            <div style={{ display: 'flex', justifyContent: 'center', gap: 20, alignItems: 'center' }}>
            <Form.Control
                type="text"
                name="name"
                placeholder="Name"
                value={itemData.name}
                onChange={handleInputChange}
                style={{ border: 'gray solid' }}
                />
                <Form.Control
                type="text"
                name="description"
                placeholder="Description"
                value={itemData.description}
                onChange={handleInputChange}
                style={{ border: 'gray solid' }}
                />
                <Form.Control
                type="number"
                name="price"
                placeholder="Price"
                value={itemData.price}
                onChange={handleInputChange}
                style={{ border: 'gray solid' }}
                />
                <Button onClick={() => addProduct()}>Update</Button>
            </div>
        </div>
    )
}

export default EditProductComponent;