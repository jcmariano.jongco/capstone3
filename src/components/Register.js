import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const  Register = () => {
  const [formData, setFormData] = useState({
    username: '',
    email: '',
    password: '',
  });
  const navigate = useNavigate();

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleRegistration = () => {
    // Validate and save user data (in this example, save to local storage)
    if (formData.username && formData.email && formData.password) {
      const users = JSON.parse(localStorage.getItem('users')) || [];
      users.push(formData);
      localStorage.setItem('users', JSON.stringify(users));
      alert('Registration successful');
      navigate('/');
      // Redirect or navigate to another page
    } else {
      alert('Please fill in all fields');
    }
  };

  return (
    <div>
      <h2>Register</h2>
      <form style={{display: 'flex', gap: 10}}>
        <input
          type="text"
          name="username"
          placeholder="Username"
          value={formData.username}
          onChange={handleInputChange}
        />
        <input
          type="email"
          name="email"
          placeholder="Email"
          value={formData.email}
          onChange={handleInputChange}
        />
        <input
          type="password"
          name="password"
          placeholder="Password"
          value={formData.password}
          onChange={handleInputChange}
        />
        <button onClick={handleRegistration}>Register</button>
      </form>
    </div>
  );
}

export default Register;
