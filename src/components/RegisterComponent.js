import React, { useState } from 'react';
import { useRegisterMutation } from '../services/registerApi';
import { useNavigate } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


const RegisterComponent = () => {
  // Define state variables for each input field
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobile, setMobile] = useState('');
  const [password, setPassword] = useState('');
  const [register] = useRegisterMutation();
  const navigate = useNavigate();

  // Handle input changes and update the state
  const handleFirstNameChange = (event) => {
    setFirstName(event.target.value);
  };

  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handleMobileChange = (event) => {
    setMobile(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  // Handle form submission
  const handleRegistration = async () => {
    try {
        const credentails = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            mobileNo: mobile,
            password: password
        }
        console.log(credentails);
        await register(credentails);

        // Handle successful login with the access token

        alert('Register successful');
        navigate('/')
      } catch (error) {
        // Handle login error, e.g., show an error message to the user
        console.error('Login error:', error.message);
      }
  };

  return (
    <div style={{margin: '10rem'}}>
          <div style={{ display: 'flex', justifyContent: 'flex-start', gap: 5, flexDirection: 'column', alignItems: 'center' }}>
      <div style={{ display: 'flex', justifyItems: 'center', gap: 5, width: '30rem'}}>
      <Form.Label htmlFor="firstname" style={{paddingRight: '4rem'}}>FirstName</Form.Label>
        <Form.Control type="text" name="firstname" value={firstName} onChange={handleFirstNameChange} />
      </div>
      <div style={{ display: 'flex', justifyItems: 'center', gap: 5, width: '30rem'}}>
      <Form.Label htmlFor="lastname" style={{paddingRight: '4rem'}}>LastName</Form.Label>
        <Form.Control type='text' name="lastname" value={lastName} onChange={handleLastNameChange} />
      </div>
      <div style={{ display: 'flex', justifyItems: 'center', gap: 5, width: '30rem'}}>
      <Form.Label htmlFor="email" style={{paddingRight: '6rem'}}>Email</Form.Label>
        <Form.Control type='email' name="email" value={email} onChange={handleEmailChange} />
      </div>
      <div style={{ display: 'flex', justifyItems: 'center', gap: 5, width: '30rem'}}>
      <Form.Label htmlFor="mobile" style={{paddingRight: '5rem'}}>Mobile</Form.Label>
        <Form.Control type='number' name="mobile" value={mobile} onChange={handleMobileChange} />
      </div>
      <div style={{ display: 'flex', justifyItems: 'center', gap: 5, width: '30rem'}}>
      <Form.Label htmlFor="password" style={{paddingRight: '4rem'}}>Password</Form.Label>
        <Form.Control type='password' name="password" value={password} onChange={handlePasswordChange} />
      </div>
      <div style={{marginTop: '1rem'}}>
      <Button variant="secondary" onClick={handleRegistration}>Register</Button>
      </div>
    </div>
    </div>
  );
};

export default RegisterComponent;
