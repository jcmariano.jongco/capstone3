import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useLoginMutation } from '../services/authApi';
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from '../store/reducers/authSlice';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
function Login() {
  const [loginData, setLoginData] = useState({
    email: '',
    password: '',
  });
  const [login] = useLoginMutation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const authData =useSelector((state) => state.auth)

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setLoginData({
      ...loginData,
      [name]: value,
    });
  };
  console.log('GLOBAL',  authData);
  React.useEffect(() => {
    // Check if the access token exists in localStorage when the component loads
    const storedAccess = localStorage.getItem('access');
    const storedEmail = localStorage.getItem('email');
    const storedAdmin = localStorage.getItem('admin');
    if (storedAccess) {
      // Set the access token in Redux state
      dispatch(setUser({ access: storedAccess, email: storedEmail, isAdmin: storedAdmin}));
    }
  }, [dispatch]);

  const handleLogin = async () => {
    try {
      const credentails = loginData;
      const data = await login(credentails).unwrap();
      const { access, email, id } = data;
      const isAdmin = email === 'admin1@gmail.com' ? true : false;
      // Handle successful login with the access token
      if (access) {
        // Save the access token to both Redux and localStorage
        localStorage.setItem('access', access);
        localStorage.setItem('email', email);
        localStorage.setItem('admin', isAdmin);
        localStorage.setItem('id', id)
        dispatch(setUser({ access: access, email: email, isAdmin: isAdmin, id: id }));
        alert('Login successful');
        if(isAdmin) {
          navigate('/shop')
        } else {
          navigate('/clientshop')
        }

      }
    } catch (error) {
      // Handle login error, e.g., show an error message to the user
      console.error('Login error:', error.message);
    }
  };


  return (
    <div > 
      <div style={{margin: 'auto', width: 'fit-content', height: '5rem'}}>
      <h2>Login</h2>
      </div>
      <div style={{margin: 'auto', width: 'fit-content', height: '5rem'}}>
      <Form.Control
          type="email"
          name="email"
          placeholder="Email"
          value={loginData.email}
          onChange={handleInputChange}
          style={{ border: 'gray solid' }}
        />
      </div>
      <div style={{margin: 'auto', width: 'fit-content', height: '5rem'}}>
        <Form.Control
          type="password"
          name="password"
          placeholder="Password"
          value={loginData.password}
          onChange={handleInputChange}
          style={{ border: 'gray solid' }}
        />
      </div>
      <div style={{margin: 'auto', width: 'fit-content', height: '5rem', gap: 30, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
      <Nav.Link href={"/register"}>Register</Nav.Link>
      <Button onClick={() => handleLogin()}>Login</Button>
      </div>
    </div>
  );
}

export default Login;
