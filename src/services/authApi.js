import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const authApi = createApi({
    reducerPath: 'authApi',
    baseQuery: fetchBaseQuery({
        // baseUrl: 'http://localhost:4000',
        baseUrl: 'https://cpstn2-ecommerceapi-jcmariano.onrender.com',
        prepareHeaders: (headers, {getState}) => {
            const { access } = getState().auth;
            if (access) {
                headers.set('authorization', `Bearer ${access}`);
            }
            headers.set('Content-Type', 'application/json');
            return headers;
        },
    }),
    endpoints: (builder) => ({
        login: builder.mutation({
            query: (credentials) => ({
                url: '/users/login',
                method: 'POST',
                body: credentials
            }),
        }),
    }),
});

export const { useLoginMutation } = authApi;