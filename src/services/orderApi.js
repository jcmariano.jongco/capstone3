import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const orderApi = createApi({
    reducerPath: 'orderApi',
    baseQuery: fetchBaseQuery({
        // baseUrl: 'http://localhost:4000',
        baseUrl: 'https://cpstn2-ecommerceapi-jcmariano.onrender.com',
        prepareHeaders: (headers, {getState}) => {
            const { access } = getState().auth;
            console.log(access);
            if (access) {
                headers.set('authorization', `Bearer ${access}`);
            }
            headers.set('Content-Type', 'application/json');
            return headers;
        }
    }),
    endpoints: builder => ({
        getOrder: builder.query({
            query: params => ({
                url: '/orders/getorders',
                method: 'GET',
                params: {}
            }),
        }),
        checkOut: builder.mutation({
            query: data => ({
                url: '/orders/checkout',
                method: 'POST',
                body: data
            }),
        }),
    }),
});

export const { useGetOrderQuery, useCheckOutMutation } = orderApi;

