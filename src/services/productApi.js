import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const productApi = createApi({
    reducerPath: 'productApi',
    baseQuery: fetchBaseQuery({
        // baseUrl: 'http://localhost:4000',
        baseUrl: 'https://cpstn2-ecommerceapi-jcmariano.onrender.com',
        prepareHeaders: (headers, {getState}) => {
            const { access } = getState().auth;
            console.log(access);
            if (access) {
                headers.set('authorization', `Bearer ${access}`);
            }
            headers.set('Content-Type', 'application/json');
            return headers;
        }
    }),
    endpoints: builder => ({
        getProduct: builder.query({
            query: params => ({
                url: '/products/retrieveproduct',
                method: 'GET',
                params: {}
            }),
        }),
        addProducts: builder.mutation({
            query: data => ({
                url: '/products/addproduct',
                method: 'POST',
                body: data
            }),
        }),
        updateProducts: builder.mutation({
            query: data => ({
                url: `/products/${data.id}`,
                method: 'PUT',
                body: {productName: data.name, description: data.description, price: data.price, id: data._id, isActive: data.isActive},
            }),
        }),
        deleteProducts: builder.mutation({
            query: data => ({
                url: `/products/archive/${data.id}`,
                method: 'DELETE',
            }),
        }),
        getActiveProduct: builder.query({
            query: params => ({
                url: '/products/active-product',
                method: 'GET',
                params: {}
            }),
        }),
    }),
});

export const { useGetProductQuery, useAddProductsMutation, useUpdateProductsMutation, useDeleteProductsMutation, useGetActiveProductQuery } = productApi;

