import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const registerApi = createApi({
    reducerPath: 'registerApi',
    baseQuery: fetchBaseQuery({
        // baseUrl: 'http://localhost:4000',
        baseUrl: 'https://cpstn2-ecommerceapi-jcmariano.onrender.com',
        prepareHeaders: (headers, {getState}) => {
            const { access } = getState().auth;
            if (access) {
                headers.set('authorization', `Bearer ${access}`);
            }
            headers.set('Content-Type', 'application/json');
            return headers;
        },
    }),
    endpoints: (builder) => ({
        register: builder.mutation({
            query: (credentials) => ({
                url: '/users/register',
                method: 'POST',
                body: credentials
            }),
        }),
    }),
});

export const { useRegisterMutation } = registerApi;