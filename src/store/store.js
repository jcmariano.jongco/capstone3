import {configureStore} from '@reduxjs/toolkit';
import {setupListeners} from '@reduxjs/toolkit/query/react';
import authReducer from './reducers/authSlice';
import productReducer from './reducers/productSlice';
import orderReducer from './reducers/orderSlice';
import registerReducer from './reducers/registerSlice';
import { authApi } from '../services/authApi';
import { productApi } from '../services/productApi';
import { orderApi } from '../services/orderApi';
import { registerApi } from '../services/registerApi';


const store = configureStore({
reducer: {
    auth: authReducer,
    product: productReducer,
    order: orderReducer,
    register: registerReducer,
    [authApi.reducerPath]: authApi.reducer,
    [productApi.reducerPath]: productApi.reducer,
    [orderApi.reducerPath]: orderApi.reducer,
    [registerApi.reducerPath]: registerApi.reducer
},
middleware: getDefaultMiddleware => getDefaultMiddleware().concat(authApi.middleware).concat(productApi.middleware).concat(orderApi.middleware).concat(registerApi.middleware)
});

setupListeners(store.dispatch);

export default store;