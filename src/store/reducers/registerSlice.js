import { createSlice } from '@reduxjs/toolkit';

const registerSlice = createSlice({
    name: 'register',
    initialState: {
            firstName: null,
            lastName: null,
            email: null,
            mobileNo: null,
            password: null
    },
    reducers: {
        setRegister: (state, action) => {
            state.firstName = action.payload.firstName;
            state.lastName = action.payload.lastName;
            state.email = action.payload.email;
            state.mobileNo = action.payload.mobileNo;
            state.password = action.payload.password
        }
    }
});

export const { setRegister } = registerSlice.actions;

export default registerSlice.reducer;