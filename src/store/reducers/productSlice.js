import { createSlice } from '@reduxjs/toolkit';

const productSlice = createSlice({
    name: 'product',
    initialState: {
        productData: [],
    },
    reducers: {
        setProduct: (state, action) => {
            state.productData = action.payload.productData
        }
    }
});

export const { setProduct } = productSlice.actions;

export default productSlice.reducer;