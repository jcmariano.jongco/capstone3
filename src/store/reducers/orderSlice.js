import { createSlice } from '@reduxjs/toolkit';

const orderSlice = createSlice({
    name: 'order',
    initialState: {
        orderData: [],
    },
    reducers: {
        setOrder: (state, action) => {
            state.orderData = action.payload.orderData
        }
    }
});

export const { setOrder } = orderSlice.actions;

export default orderSlice.reducer;