import { createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        id: null,
        username: null,
        access: null,
        email: null,
        isAdmin: false
    },
    reducers: {
        setUser: (state, action) => {
            state.id = action.payload.id;
            state.access = action.payload.access;
            state.email = action.payload.email;
            state.isAdmin = action.payload.isAdmin;
        }
    }
});

export const { setUser } = authSlice.actions;

export default authSlice.reducer;