import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from '../src/components/Home';
import Product from '../src/components/Product';
import Cart from '../src/components/Cart';
import Login from '../src/components/Login';
import AddProductComponent from './components/AddProducts';
import { Provider } from 'react-redux';
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';


import store from './store/store'
import RegisterComponent from './components/RegisterComponent';
import HomeClient from './components/HomeClient';

function App() {
  return (
    <Provider store={store}>
      <Container>
          <Router>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/shop" element={<Home />} />
        <Route path="/clientshop" element={<HomeClient />} />
        <Route path="/product/:id" element={<Product />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/register" element={<RegisterComponent />} />
        <Route path='/addproduct' element={<AddProductComponent />} />
      </Routes>
    </Router>
    </Container>
    </Provider>
  );
}

export default App;
